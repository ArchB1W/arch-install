#!/bin/sh
echo -e '
# Please select your kernel\n
linux
linux-lts
linux-zen
linux-hardened\n'

# Ask for kern
read kern

# Void
echo -e '\n'

# Install base
pacstrap /mnt base base-devel $kern $kern-headers linux-firmware netctl networkmanager dhcpcd openssh nano sudo git man pacman-contrib reflector

# Gen Fstab
genfstab -U /mnt >> /mnt/etc/fstab

mkdir -p /mnt/arch_install
cp -r $PWD/../* /mnt/arch_install

arch-chroot /mnt
