#!/bin/sh

# Root passwd
echo 'Set root passwd'
passwd

echo 'CPU
----------
amd
intel
----------'
echo
read cpu

echo 'ucode chosen'

echo "root drive
----------
$(lsblk)
----------"
read $drive

uuid=$(sudo blkid ${drive} | awk '{print $2}' | sed 's/\"//g')

# Install grub and ucode
pacman -S refind ${cpu}-ucode

refind-install --alldrivers $drive

sudo cp /boot/EFI/BOOT/refind.conf-sample /boot/EFI/BOOT/refind.conf

# Request exit and reboot
echo 'You may now either Exit then reboot or head on to the extras directory'
