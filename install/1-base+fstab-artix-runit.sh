#!/bin/sh
echo '
# 1 Is to install Artix they are the same with a different kernel
# If using standard linux kernel input ' '
linux
linux-lts
linux-zen
linux-hardened'

# Ask for kern
read kern

# Void
echo -e '\n'

# Install base
basestrap /mnt base base-devel $kern $kern-headers linux-firmware networkmanager-runit reflector openssh nano sudo git man pacman-contrib runit elogind-runit

# Gen Fstab
fstabgen -U /mnt >> /mnt/etc/fstab

mkdir -p /mnt/arch_install
cp -r $PWD/../* /mnt/arch_install

artools-chroot /mnt
