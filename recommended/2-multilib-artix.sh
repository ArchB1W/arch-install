#!/bin/sh
# Check for root
if [ "$(whoami)" != root ]; then
    echo Please run this script as root or using sudo
    exit
fi

# Uncomment line 92, 93, 120, and 121 for lib32 and multilib from /etc/pacman.conf
sed -i -e '92s/^.\(.*\)/\1/' -e '93s/^.\(.*\)/\1/' -e '120s/^.\(.*\)/\1/' -e '121s/^.\(.*\)/\1/' /etc/pacman.conf
