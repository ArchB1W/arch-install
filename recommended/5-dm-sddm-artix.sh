#!/bin/sh
# Check for root
if [ `whoami` != root ]; then
    echo Please run this script as root or using sudo
    exit
fi

pacman -S sddm-runit --noconfirm
ln -sf /etc/runit/sv/sddm /etc/runit/runsvdir/current
